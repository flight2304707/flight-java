package com.tracking.flight.controller;

import com.tracking.flight.model.bean.OpenSkyFilters;
import com.tracking.flight.model.mongo.FlightDataOpenSky;
import com.tracking.flight.service.OpenSkyApiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/api/open-sky")
public class OpenSkyApiController {

    private final OpenSkyApiService openSkyApiService;

    @Autowired
    public OpenSkyApiController(OpenSkyApiService openSkyApiService) {
        this.openSkyApiService = openSkyApiService;
    }

    @GetMapping
    public ResponseEntity<FlightDataOpenSky> getAll() {
        return new ResponseEntity<>(openSkyApiService.getAll(), HttpStatus.OK);
    }

    @GetMapping("/filters")
    public ResponseEntity<FlightDataOpenSky> search(@ModelAttribute OpenSkyFilters openSkyFilters) {
        return new ResponseEntity<>(openSkyApiService.getWithFilter(openSkyFilters), HttpStatus.OK);
    }
}
