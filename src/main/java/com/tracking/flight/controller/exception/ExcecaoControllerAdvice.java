package com.tracking.flight.controller.exception;

import com.tracking.flight.exception.ExcecaoGenerica;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.LocalDateTime;
import java.util.LinkedHashMap;
import java.util.Map;

@ControllerAdvice
public class ExcecaoControllerAdvice extends ResponseEntityExceptionHandler {

    @ExceptionHandler(ExcecaoGenerica.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ResponseEntity<Map<String, Object>> handleExcecaoGenerica(ExcecaoGenerica ex) {
        Map<String, Object> responseBody = new LinkedHashMap<>();
        responseBody.put("message", ex.getMessage());
        responseBody.put("timestamp", LocalDateTime.now());
        responseBody.put("status", ex.getHttpStatus().value());

        return new ResponseEntity<>(responseBody, ex.getHttpStatus());
    }
}
