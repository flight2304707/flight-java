package com.tracking.flight.utils;

public class ValidatorUtils {
    public static boolean isValidIcao24(String icao24) {
        if (icao24 == null || icao24.length() != 6) {
            return false;
        }

        for (char ch : icao24.toCharArray()) {
            if (!Character.isDigit(ch) && (ch < 'A' || ch > 'F') && (ch < 'a' || ch > 'f')) {
                return false;
            }
        }

        return true;
    }
}
