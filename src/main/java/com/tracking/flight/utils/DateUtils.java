package com.tracking.flight.utils;

import java.time.Duration;
import java.time.Instant;

public class DateUtils {

    public static Duration getDurationLastTrack(Integer time) {
        Instant timestampInstant = Instant.ofEpochSecond(time);
        Instant now = Instant.now();

        return Duration.between(timestampInstant, now);
    }
}
