package com.tracking.flight.utils;

import java.util.Set;

public class ConstUtils {

    public static final Set<String> keyParams = Set.of(
            "time",
            "icao24",
            "lamin",
            "lomin",
            "lamax",
            "lomax",
            "extended",
            "mongo",
            "country"
    );

}
