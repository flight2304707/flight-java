package com.tracking.flight.exception;

import org.springframework.http.HttpStatus;

public class ExcecaoGenerica extends RuntimeException {
    private final HttpStatus httpStatus;

    public ExcecaoGenerica(HttpStatus httpStatus, String mensagem) {
        super(mensagem);
        this.httpStatus = httpStatus;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }
}
