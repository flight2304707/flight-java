package com.tracking.flight.repository.mongo;

import com.tracking.flight.model.mongo.FlightDataOpenSky;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface FlightDataOpenSkyMongoRepository extends MongoRepository<FlightDataOpenSky, String> {
    Optional<FlightDataOpenSky> findTopByOrderByTimeDesc();
}
