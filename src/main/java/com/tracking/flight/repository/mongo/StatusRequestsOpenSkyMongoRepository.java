package com.tracking.flight.repository.mongo;

import com.tracking.flight.model.mongo.StatusRequestsOpenSkyMongo;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface StatusRequestsOpenSkyMongoRepository extends MongoRepository<StatusRequestsOpenSkyMongo, String> {
}
