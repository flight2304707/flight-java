package com.tracking.flight.service;

import com.tracking.flight.exception.ExcecaoGenerica;
import com.tracking.flight.model.mongo.FlightDataOpenSky;
import com.tracking.flight.model.mongo.FlightStateOpenSky;
import com.tracking.flight.repository.mongo.FlightDataOpenSkyMongoRepository;
import com.tracking.flight.utils.DateUtils;
import com.tracking.flight.utils.ValidatorUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.MongoTransactionException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.util.List;

@Service
public class FlightDataOpenSkyService {

    private final FlightDataOpenSkyMongoRepository flightDataOpenSkyMongoRepository;

    private final FlightStateOpenSkyService flightStateOpenSkyService;

    @Autowired
    public FlightDataOpenSkyService(FlightDataOpenSkyMongoRepository flightDataOpenSkyMongoRepository, FlightStateOpenSkyService flightStateOpenSkyService) {
        this.flightDataOpenSkyMongoRepository = flightDataOpenSkyMongoRepository;
        this.flightStateOpenSkyService = flightStateOpenSkyService;
    }

    public FlightDataOpenSky saveFlightData(FlightDataOpenSky flightDataOpenSky) {
        try {
            return flightDataOpenSkyMongoRepository.save(flightDataOpenSky);
        } catch (MongoTransactionException mongoTransactionException) {
            throw new ExcecaoGenerica(HttpStatus.INTERNAL_SERVER_ERROR, "MONGO NÃO SALVOU!");
        } catch (Exception e) {
            throw new ExcecaoGenerica(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
        }
    }

    public FlightDataOpenSky lastUpdateFlightData() {
        return flightDataOpenSkyMongoRepository.findTopByOrderByTimeDesc()
                .orElse(null);
    }

    /**
     * BUSCA DADOS DO VOO PELO CODIGO ICAO24-BIT
     * @param icao24
     * @return
     */
    public FlightDataOpenSky getFlightDataByAircraft(String icao24) {
        if (ValidatorUtils.isValidIcao24(icao24)) {
            FlightDataOpenSky flightDataOpenSky = flightDataOpenSkyMongoRepository.findTopByOrderByTimeDesc()
                    .orElseThrow(() -> new ExcecaoGenerica(HttpStatus.NOT_FOUND, "Nenhum tracking encontrado!"));

            List<FlightStateOpenSky> filteredList = flightDataOpenSky.getStates().stream()
                    .filter(flight -> icao24.equals(flight.getIcao24()))
                    .toList();

            Duration duration = DateUtils.getDurationLastTrack(flightDataOpenSky.getTime());

            flightStateOpenSkyService.updatePosition(filteredList.get(0), duration.getSeconds());

            flightDataOpenSky.setStates(filteredList);
            flightDataOpenSky.setPrevious(true);

            return flightDataOpenSky;
        }

        throw new ExcecaoGenerica(HttpStatus.BAD_REQUEST, "ICAO24 Inválido!");
    }

    public FlightDataOpenSky getFlightDataByCountry(String country) {
        FlightDataOpenSky flightDataOpenSky = flightDataOpenSkyMongoRepository.findTopByOrderByTimeDesc()
                .orElseThrow(() -> new ExcecaoGenerica(HttpStatus.NOT_FOUND, "Nenhum tracking encontrado!"));

        List<FlightStateOpenSky> filteredList = flightDataOpenSky.getStates().stream()
                .filter(flight -> country.equals(flight.getOriginCountry()))
                .toList();

        Duration duration = DateUtils.getDurationLastTrack(flightDataOpenSky.getTime());

        flightStateOpenSkyService.updatePosition(filteredList.get(0), duration.getSeconds());

        flightDataOpenSky.setStates(filteredList);
        flightDataOpenSky.setPrevious(true);

        return flightDataOpenSky;
    }
}
