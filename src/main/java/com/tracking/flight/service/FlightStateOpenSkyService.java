package com.tracking.flight.service;

import com.tracking.flight.model.mongo.FlightStateOpenSky;
import org.springframework.stereotype.Service;

@Service
public class FlightStateOpenSkyService {

    public void updatePosition(FlightStateOpenSky flight, long timeInSeconds) {
        double velocityMps = flight.getVelocity() * 0.514444;
        double distance = velocityMps * timeInSeconds;
        double earthRadius = 6371000;

        double latRad = Math.toRadians(flight.getLatitude());
        double lonRad = Math.toRadians(flight.getLongitude());
        double trackRad = Math.toRadians(flight.getTrueTrack());

        double newLatRad = Math.asin(Math.sin(latRad) * Math.cos(distance / earthRadius) +
                Math.cos(latRad) * Math.sin(distance / earthRadius) * Math.cos(trackRad));

        double newLonRad = lonRad + Math.atan2(Math.sin(trackRad) * Math.sin(distance / earthRadius) * Math.cos(latRad),
                Math.cos(distance / earthRadius) - Math.sin(latRad) * Math.sin(newLatRad));

        double newLatitude = Math.round(Math.toDegrees(newLatRad) * 10000) / 10000.0;
        double newLongitude = Math.round(Math.toDegrees(newLonRad) * 10000) / 10000.0;

        flight.setLatitude(newLatitude);
        flight.setLongitude(newLongitude);
        flight.setAltitude(flight.getAltitude() + flight.getVerticalRate() * timeInSeconds);
    }
}
