package com.tracking.flight.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tracking.flight.exception.ExcecaoGenerica;
import com.tracking.flight.model.bean.OpenSkyFilters;
import com.tracking.flight.model.mongo.FlightDataOpenSky;
import com.tracking.flight.model.mongo.FlightStateOpenSky;
import com.tracking.flight.model.mongo.StatusRequestsOpenSkyMongo;
import com.tracking.flight.utils.ConstUtils;
import com.tracking.flight.utils.DateUtils;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.lang.reflect.Field;
import java.net.SocketException;
import java.time.Duration;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

@Service
public class OpenSkyApiService {

    @Value("${open-sky-url}")
    private String openSkyUrl;

    @Value("${open-sky-duration-new-search}")
    private Integer DURATION_TIME_FOR_NEW_SEARCH;

    private String getAll = "/states/all";

    private long timeInSeconds = 0;

    private final OkHttpClient client = new OkHttpClient().newBuilder()
            .connectTimeout(60, TimeUnit.SECONDS)
            .readTimeout(60, TimeUnit.SECONDS)
            .writeTimeout(60, TimeUnit.SECONDS)
            .build();

    private final ObjectMapper objectMapper = new ObjectMapper();

    private final FlightDataOpenSkyService flightDataOpenSkyService;

    private final FlightStateOpenSkyService flightStateOpenSkyService;

    private final StatusRequestsOpenSkyService statusRequestsOpenSkyService;

    @Autowired
    public OpenSkyApiService(FlightDataOpenSkyService flightDataOpenSkyService, FlightStateOpenSkyService flightStateOpenSkyService, StatusRequestsOpenSkyService statusRequestsOpenSkyService) {
        this.flightDataOpenSkyService = flightDataOpenSkyService;
        this.flightStateOpenSkyService = flightStateOpenSkyService;
        this.statusRequestsOpenSkyService = statusRequestsOpenSkyService;
    }

    public FlightDataOpenSky getAll() {
        FlightDataOpenSky flightDataOpenSky = validateLastFlightData();

        if (nonNull(flightDataOpenSky)) return flightDataOpenSky;

        flightDataOpenSky = new FlightDataOpenSky();

        Request request = new Request.Builder()
                .url(getUrlBuilder().toString())
                .build();

        try (Response response = client.newCall(request).execute()) {
            if (response.isSuccessful()) {
                try {
                    Map<String, Object> responseData = objectMapper.readValue(response.body().string(), new TypeReference<Map<String, Object>>() {
                    });

                    Integer time = (Integer) responseData.get("time");

                    List<List<Object>> states = (List<List<Object>>) responseData.get("states");
                    List<FlightStateOpenSky> flightStateOpenSkies = convertToFlightStates(states);

                    flightDataOpenSky.setTime(time);
                    flightDataOpenSky.setPrevious(false);
                    flightDataOpenSky.setStates(flightStateOpenSkies);

                    this.statusRequestsOpenSkyService.saveStatusRequest(
                            prepareBodyStatusRequests(
                                    "success", response.code()
                            )
                    );

                    response.close();

                    return flightDataOpenSkyService.saveFlightData(flightDataOpenSky);
                } catch (Exception e) {
                    this.statusRequestsOpenSkyService.saveStatusRequest(
                            prepareBodyStatusRequests(
                                    e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR.value()
                            )
                    );

                    throw new ExcecaoGenerica(HttpStatus.INTERNAL_SERVER_ERROR, "API FAIL!");
                }
            } else {
                this.statusRequestsOpenSkyService.saveStatusRequest(
                        prepareBodyStatusRequests(
                                "fail", response.code()
                        )
                );

                FlightDataOpenSky flightData = flightDataOpenSkyService.lastUpdateFlightData();

                if (isNull(flightData)) {
                    throw new ExcecaoGenerica(HttpStatus.INTERNAL_SERVER_ERROR, "API FAIL - NO DATA!");
                }

                return positionsPredictions(flightDataOpenSkyService.lastUpdateFlightData());
            }
        } catch (SocketException socketException) {
            this.statusRequestsOpenSkyService.saveStatusRequest(
                    prepareBodyStatusRequests(
                            socketException.getMessage(), HttpStatus.REQUEST_TIMEOUT.value()
                    )
            );

            throw new ExcecaoGenerica(HttpStatus.REQUEST_TIMEOUT, socketException.getMessage());
        } catch (IOException e) {
            this.statusRequestsOpenSkyService.saveStatusRequest(
                    prepareBodyStatusRequests(
                            e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR.value()
                    )
            );

            throw new ExcecaoGenerica(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
        }
    }

    public FlightDataOpenSky getWithFilter(OpenSkyFilters openSkyFilters) {
        if (openSkyFilters.getMongo()) {
            if (nonNull(openSkyFilters.getIcao24())) {
                return flightDataOpenSkyService.getFlightDataByAircraft(openSkyFilters.getIcao24());
            } else if (nonNull(openSkyFilters.getCountry())) {
                return flightDataOpenSkyService.getFlightDataByCountry(openSkyFilters.getCountry());
            }
        }

        FlightDataOpenSky flightDataOpenSky = new FlightDataOpenSky();

        Request request = new Request.Builder()
                .url(prepareUrlWithFilters(openSkyFilters))
                .build();

        try (Response response = client.newCall(request).execute()) {
            if (response.isSuccessful()) {
                try {
                    Map<String, Object> responseData = objectMapper.readValue(response.body().string(), new TypeReference<Map<String, Object>>() {
                    });

                    Integer time = (Integer) responseData.get("time");

                    List<List<Object>> states = (List<List<Object>>) responseData.get("states");
                    List<FlightStateOpenSky> flightStateOpenSkies = convertToFlightStates(states);

                    flightDataOpenSky.setTime(time);
                    flightDataOpenSky.setPrevious(false);
                    flightDataOpenSky.setStates(flightStateOpenSkies);

                    this.statusRequestsOpenSkyService.saveStatusRequest(
                            prepareBodyStatusRequests(
                                    "success", response.code()
                            )
                    );

                    response.close();

                    return flightDataOpenSky;
                } catch (Exception e) {
                    this.statusRequestsOpenSkyService.saveStatusRequest(
                            prepareBodyStatusRequests(
                                    e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR.value()
                            )
                    );

                    throw new ExcecaoGenerica(HttpStatus.INTERNAL_SERVER_ERROR, "API FAIL!");
                }
            }
        } catch (SocketException socketException) {
            this.statusRequestsOpenSkyService.saveStatusRequest(
                    prepareBodyStatusRequests(
                            socketException.getMessage(), HttpStatus.REQUEST_TIMEOUT.value()
                    )
            );

            throw new ExcecaoGenerica(HttpStatus.REQUEST_TIMEOUT, socketException.getMessage());
        } catch (IOException e) {
            this.statusRequestsOpenSkyService.saveStatusRequest(
                    prepareBodyStatusRequests(
                            e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR.value()
                    )
            );

            throw new ExcecaoGenerica(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
        }

        return flightDataOpenSky;
    }

    private StatusRequestsOpenSkyMongo prepareBodyStatusRequests(String message, Integer code) {
        return new StatusRequestsOpenSkyMongo(
                code,
                message,
                new Date()
        );
    }

    private HttpUrl.Builder getUrlBuilder() {
        return HttpUrl.parse(openSkyUrl + getAll).newBuilder();
    }

    private String prepareUrlWithFilters(OpenSkyFilters openSkyFilters) {
        HttpUrl.Builder url = getUrlBuilder();

        for (Field field : OpenSkyFilters.class.getDeclaredFields()) {
            field.setAccessible(true);
            try {
                Object value = field.get(openSkyFilters);
                if (nonNull(value)) {
                    url.addQueryParameter(field.getName(), value.toString());
                }
            } catch (IllegalAccessException e) {
                System.out.println(e.getMessage());
            }
        }

        return url.build().toString();
    }

    private void validateFilters(Map<String, String> allParams) {
        Set<String> invalidKeys = allParams.keySet().stream()
                .filter(key -> !ConstUtils.keyParams.contains(key))
                .collect(Collectors.toSet());

        if (!invalidKeys.isEmpty()) {
            throw new ExcecaoGenerica(HttpStatus.BAD_REQUEST, "Filtro não reconhecido!");
        }
    }

    private FlightDataOpenSky validateLastFlightData() {
        FlightDataOpenSky flightDataOpenSky = flightDataOpenSkyService.lastUpdateFlightData();

        if (isNull(flightDataOpenSky)) return null;

        Duration duration = DateUtils.getDurationLastTrack(flightDataOpenSky.getTime());

        timeInSeconds = duration.getSeconds();

        System.out.println("TEMPO DA ULTIMA REQUEST: " + duration.toMinutes());

        if (duration.toMinutes() >= DURATION_TIME_FOR_NEW_SEARCH) {
            return null;
        }

        return positionsPredictions(flightDataOpenSky);
    }

    private FlightDataOpenSky positionsPredictions(FlightDataOpenSky flightDataOpenSky) {
        for (FlightStateOpenSky flightStateOpenSky : flightDataOpenSky.getStates()) {
            if (nonNull(flightStateOpenSky.getAltitude())
                    && nonNull(flightStateOpenSky.getVerticalRate())
                    && nonNull(flightStateOpenSky.getLatitude())
                    && nonNull(flightStateOpenSky.getLongitude())
                    && nonNull(flightStateOpenSky.getTrueTrack())) {
                flightStateOpenSkyService.updatePosition(flightStateOpenSky, timeInSeconds);
            }
        }

        flightDataOpenSky.setPrevious(true);

        return flightDataOpenSkyService.saveFlightData(flightDataOpenSky);
    }

    private List<FlightStateOpenSky> convertToFlightStates(List<List<Object>> states) {
        if (isNull(states)) throw new ExcecaoGenerica(HttpStatus.NOT_FOUND, "Nenhum dado encontrado!");

        List<FlightStateOpenSky> statesList = new ArrayList<>();

        for (List<Object> state : states) {
            FlightStateOpenSky flightStateOpenSky = new FlightStateOpenSky(
                    (String) state.get(0),
                    (String) state.get(1),
                    (String) state.get(2),
                    (state.get(3) != null) ? ((Number) state.get(3)).longValue() : null,
                    (state.get(4) != null) ? ((Number) state.get(4)).longValue() : null,
                    (state.get(5) != null) ? ((Number) state.get(5)).doubleValue() : null,
                    (state.get(6) != null) ? ((Number) state.get(6)).doubleValue() : null,
                    (state.get(7) != null) ? ((Number) state.get(7)).doubleValue() : null,
                    (Boolean) state.get(8),
                    (state.get(9) != null) ? ((Number) state.get(9)).doubleValue() : null,
                    (state.get(10) != null) ? ((Number) state.get(10)).doubleValue() : null,
                    (state.get(11) != null) ? ((Number) state.get(11)).doubleValue() : null,
                    null,
                    (state.get(13) != null) ? ((Number) state.get(13)).doubleValue() : null,
                    (String) state.get(14),
                    (Boolean) state.get(15),
                    (state.get(16) != null) ? ((Number) state.get(16)).intValue() : null
            );

            statesList.add(flightStateOpenSky);
        }

        return statesList;
    }
}
