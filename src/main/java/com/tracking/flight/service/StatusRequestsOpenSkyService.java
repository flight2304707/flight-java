package com.tracking.flight.service;

import com.tracking.flight.model.mongo.StatusRequestsOpenSkyMongo;
import com.tracking.flight.repository.mongo.StatusRequestsOpenSkyMongoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StatusRequestsOpenSkyService {

    private final StatusRequestsOpenSkyMongoRepository statusRequestsOpenSkyMongoRepository;

    @Autowired
    public StatusRequestsOpenSkyService(StatusRequestsOpenSkyMongoRepository statusRequestsOpenSkyMongoRepository) {
        this.statusRequestsOpenSkyMongoRepository = statusRequestsOpenSkyMongoRepository;
    }

    public void saveStatusRequest(StatusRequestsOpenSkyMongo statusRequestsOpenSkyMongo) {
        statusRequestsOpenSkyMongoRepository.save(statusRequestsOpenSkyMongo);
    }
}
