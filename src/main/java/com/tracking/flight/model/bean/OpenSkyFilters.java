package com.tracking.flight.model.bean;


public class OpenSkyFilters {
    private String time;
    private String icao24;
    private String lamin;
    private String lomin;
    private String lamax;
    private String lomax;
    private String extended;
    private Boolean mongo;
    private String country;

    public OpenSkyFilters() {
    }

    public OpenSkyFilters(String time, String icao24, String lamin, String lomin, String lamax, String lomax, String extended, Boolean mongo, String country) {
        this.time = time;
        this.icao24 = icao24;
        this.lamin = lamin;
        this.lomin = lomin;
        this.lamax = lamax;
        this.lomax = lomax;
        this.extended = extended;
        this.mongo = mongo;
        this.country = country;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getIcao24() {
        return icao24;
    }

    public void setIcao24(String icao24) {
        this.icao24 = icao24;
    }

    public String getLamin() {
        return lamin;
    }

    public void setLamin(String lamin) {
        this.lamin = lamin;
    }

    public String getLomin() {
        return lomin;
    }

    public void setLomin(String lomin) {
        this.lomin = lomin;
    }

    public String getLamax() {
        return lamax;
    }

    public void setLamax(String lamax) {
        this.lamax = lamax;
    }

    public String getLomax() {
        return lomax;
    }

    public void setLomax(String lomax) {
        this.lomax = lomax;
    }

    public String getExtended() {
        return extended;
    }

    public void setExtended(String extended) {
        this.extended = extended;
    }

    public Boolean getMongo() {
        return mongo;
    }

    public void setMongo(Boolean mongo) {
        this.mongo = mongo;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Override
    public String toString() {
        return "OpenSkyFilters{" +
                "time='" + time + '\'' +
                ", icao24='" + icao24 + '\'' +
                ", lamin='" + lamin + '\'' +
                ", lomin='" + lomin + '\'' +
                ", lamax='" + lamax + '\'' +
                ", lomax='" + lomax + '\'' +
                ", extended='" + extended + '\'' +
                ", mongo='" + mongo + '\'' +
                ", country='" + country + '\'' +
                '}';
    }
}
