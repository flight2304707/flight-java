package com.tracking.flight.model.mongo;

import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document(value = "status_requests_open_sky")
public class StatusRequestsOpenSkyMongo {
    private Integer code;
    private String message;
    private Date dataCadastro;

    public StatusRequestsOpenSkyMongo() {
    }

    public StatusRequestsOpenSkyMongo(Integer code, String message, Date dataCadastro) {
        this.code = code;
        this.message = message;
        this.dataCadastro = dataCadastro;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getDataCadastro() {
        return dataCadastro;
    }

    public void setDataCadastro(Date dataCadastro) {
        this.dataCadastro = dataCadastro;
    }
}
