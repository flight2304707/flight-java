package com.tracking.flight.model.mongo;

import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;


@Document(collection = "all_flights_open_sky")
public class FlightDataOpenSky {
    private Integer time;
    private Boolean previous;
    private List<FlightStateOpenSky> states;

    public FlightDataOpenSky() {
    }

    public FlightDataOpenSky(Integer time, List<FlightStateOpenSky> states, Boolean previous) {
        this.time = time;
        this.states = states;
        this.previous = previous;
    }

    public Integer getTime() {
        return time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }

    public Boolean getPrevious() {
        return previous;
    }

    public void setPrevious(Boolean previous) {
        this.previous = previous;
    }

    public List<FlightStateOpenSky> getStates() {
        return states;
    }

    public void setStates(List<FlightStateOpenSky> states) {
        this.states = states;
    }
}
